# Small Es Poc

Small es pos based on update-on-query logic

To setup this project you need pipenv and docker-compose

Then just run

```
docker-compose up -d
pipenv shell
pipenv install -d
python example.py
```
