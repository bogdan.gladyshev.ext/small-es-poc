import asyncio
import datetime
import itertools
import random
import uuid

import attr
import cattrs
import opensearchpy
from opensearchpy.helpers import async_bulk
import toolz

host = 'localhost'
port = 9200
auth = ('admin', 'admin')

ORDER_IDS = []


@attr.s(slots=True, auto_attribs=True)
class User:
    id: int
    name: str
    surname: str
    modifiedTime: int = 0

    @classmethod
    def generete(cls):
        return random.choice(USERS)


USERS = [
    User(1, 'User1', 'User1'),
    User(2, 'User2', 'User2'),
    User(3, 'User3', 'User3'),
]


@attr.s(slots=True, auto_attribs=True)
class DesignedTarget:
    id: str
    name: str
    modifiedTime: int = 0

    @classmethod
    def generate(cls):
        return DesignedTarget(id=str(uuid.uuid4()), name="Initial")


@attr.s(slots=True, auto_attribs=True)
class Order:
    id: str
    user: User
    target: DesignedTarget
    modifiedTime: int = 0

    @classmethod
    def generate(cls):
        return Order(id=str(uuid.uuid4()), user=User.generete(), target=DesignedTarget.generate())


@attr.s(slots=True, auto_attribs=True)
class Retry:

    version_conflicts: int
    retry_number: int
    shift_number: int


async def ensure_index(client: opensearchpy.AsyncOpenSearch, index_name: str, order_amount: int) -> None:
    if await client.indices.exists('poc-index'):
        await client.indices.delete(index_name)
        print('\nRemove index')
    index_body = {'settings': {'index': {'number_of_shards': 4, 'number_of_replicas': 0, 'max_result_window': order_amount}}}
    response = await client.indices.create(index_name, body=index_body)
    print('\nCreating index:')
    print(response)


async def fill_orders(client: opensearchpy.AsyncOpenSearch, index_name: str, target_amount: int = 50000) -> None:
    bulk_body = []
    for _ in range(0, target_amount):
        order = Order.generate()
        ORDER_IDS.append(order.id)
        base = cattrs.unstructure(order)
        base.update({
            '_op_type': 'index',
            '_index': index_name,
            '_id': order.id,
        })
        bulk_body.append(base)
    print(f"Insert {target_amount} orders")
    print(await async_bulk(client, bulk_body))


async def state_transfers(client: opensearchpy.AsyncOpenSearch, index_name: str, slice_size: int = 2000, worker_count: int = 8) -> list[int]:

    async def internal_worker(order_ids: list[int], target_state: str) -> None:
        for order_id in order_ids:
            await client.update(index_name, order_id, {
                'doc': {
                    'target': {
                        'name': target_state,
                        'modifiedTime': datetime.datetime.now().timestamp()
                    }
                }
            })

    random.shuffle(ORDER_IDS)

    target_order_ids = ORDER_IDS[:slice_size]

    partition_size = slice_size // worker_count

    await asyncio.wait(
        [internal_worker(order_ids, 'updated') for order_ids in toolz.partition_all(partition_size, target_order_ids)]
    )

    print("Finished first updating")

    random.shuffle(target_order_ids)

    await asyncio.wait(
        [internal_worker(order_ids, 'finished') for order_ids in toolz.partition_all(partition_size, target_order_ids)]
    )

    print("Finished second updating")

    return target_order_ids


async def user_name_update(client: opensearchpy.AsyncOpenSearch, index_name: str, user_id: int, shift_count: int = 5) -> None:
    retries = []

    for shift_number in range(1, shift_count + 1):
        retry_counter = 1
        while True:
            result = await client.update_by_query(index_name, body={
                "query": {
                    "bool": {
                        "filter": [
                            {"term": {"user.id": user_id}},
                            {"term": {"user.modifiedTime": shift_number - 1}}
                        ]
                    }
                },
                "script": {
                    "lang": "painless",
                    "source": f"ctx._source.user.name = \"User{user_id}-{shift_number}\"; ctx._source.user.modifiedTime = {shift_number}"
                }
            }, conflicts='proceed', refresh=True)
            print(result)
            if result['total'] == 0:
                print("Something pretty strange")
                print(result)
            if result['version_conflicts'] == 0:
                print(f"User {user_id} shifted to {shift_number}")
                break
            else:
                retries.append(Retry(result['version_conflicts'], retry_counter, shift_number))
                retry_counter += 1
        await asyncio.sleep(5)

    return retries


async def main():
    client = opensearchpy.AsyncOpenSearch(
        hosts=[{
            'host': host,
            'port': port
        }],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth = auth,
        use_ssl = True,
        verify_certs = False,
        ssl_assert_hostname = False,
        ssl_show_warn = False,
        sniff_timeout=1800
    )

    order_amount = 50000
    slice_size = 400
    shift_count = 5

    index_name = 'poc-index'
    await ensure_index(client, index_name, order_amount)
    await fill_orders(client, index_name, target_amount=order_amount)
    transfered_order_ids, retries_1, retries_2, retries_3 = await asyncio.gather(
        state_transfers(client, index_name, slice_size=slice_size),
        user_name_update(client, index_name, 1, shift_count=shift_count),
        user_name_update(client, index_name, 2, shift_count=shift_count),
        user_name_update(client, index_name, 3, shift_count=shift_count)
    )

    # collect result data

    raw_orders = await client.search(index=index_name, size=order_amount, body={"query": {"match_all": {}}})



    orders = [
        cattrs.structure(raw_order['_source'], Order) for raw_order
        in raw_orders['hits']['hits']
    ]

    transfered_orders = 0
    transfered_order_errors = 0
    user_order_errors = 0

    for order in orders:
        if order.id in transfered_order_ids:
            if order.target.name == 'finished':
                transfered_orders += 1
            else:
                transfered_order_errors += 1
        if order.user.name != f"User{order.user.id}-{shift_count}" or order.user.modifiedTime != shift_count:
            # print(order)
            user_order_errors += 1

    print(f"Transfered count: {transfered_orders}")
    print(f"Transfered count errors: {transfered_order_errors}")
    print(f"User order errors: {user_order_errors}")

    for record in itertools.chain(retries_1, retries_2, retries_3):
        print(record)

    await client.close()


asyncio.new_event_loop().run_until_complete(main())